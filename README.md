# Network Calculator

This is a graphical network calculator for IPv4 addresses and sub-net masks.
It was inspired by the excellent [subnetcalc](https://www.uni-due.de/~be0001/subnetcalc/)
program created by [Thomas Dreibholz](mailto:dreibh@iem.uni-due.de).

## Limitations
- Only IPv4 addresses are supported (No IPv6 support).
- Only basic information is provided (No GeoIP, Country, etc.).

If you want that sort of additional information I recommend using
[subnetcalc](https://www.uni-due.de/~be0001/subnetcalc/).

## License Information
This work is released under the BSD 3-Clause license as contained in the
LICENSE file.

## Compiling
### Prerequisites
- [Lazarus](https://www.lazarus-ide.org/)

### Procedure
- Open the **netcalc.lpi** file in Lazarus
- Choose **Run -> Build** from the menu (Or press Shift+F9)

## Installation
There is nothing specific to install. Once you have compiled the project
you can copy the **netcalc** executable (**netcalc.exe** on Windows) to
any folder you like and run it from there.

## References
### [Wikipedia](https://en.wikipedia.org/wiki/Main_Page)
- [Internet Protocol version 4](https://en.wikipedia.org/wiki/Internet_Protocol_version_4)
- [Private network](https://en.wikipedia.org/wiki/Private_network)
- [Classful network](https://en.wikipedia.org/wiki/Classful_network)
- [Classless Inter-Domain Routing (CIDR)](https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing)

### [Internet Engineering Task Force (IETF)](https://www.ietf.org/)
- [RFC 1519: Classless Inter-Domain Routing (CIDR)](https://www.rfc-editor.org/rfc/rfc1519.html)
- [RFC 1878: Variable Length Subnet Table For IPv4](https://www.rfc-editor.org/rfc/rfc1878.html)
- [RFC 1918: Address Allocation for Private Internets](https://www.rfc-editor.org/rfc/rfc1918.html)
- [RFC 3330: Special-Use IPv4 Addresses](https://www.rfc-editor.org/rfc/rfc3330.html)
- [RFC 4632: Classless Inter-Domain Routing (CIDR)](https://www.rfc-editor.org/rfc/rfc4632.html)
- [RFC 6890: Special-Purpose IP Address Registries](https://www.rfc-editor.org/rfc/rfc6890.html)
