unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  LCLIntf, Spin;

type

  { TformMain }

  TformMain = class(TForm)
    btnCalculate: TButton;
    btnHelp: TButton;
    btnQuit: TButton;
    gbDetails: TGroupBox;
    IPoctet3: TSpinEdit;
    IPoctet4: TSpinEdit;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    memoDetails: TMemo;
    IPnetmask: TSpinEdit;
    IPoctet1: TSpinEdit;
    IPoctet2: TSpinEdit;
    tbNetmaskBinary: TEdit;
    LabelBIN: TLabel;
    tbNetmaskDecimal: TEdit;
    LabelDEC: TLabel;
    gbIPv4Address: TGroupBox;
    gbNetworkMask: TGroupBox;
    Label4: TLabel;
    procedure btnCalculateClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure btnQuitClick(Sender: TObject);
    procedure cbNetmaskChange(Sender: TObject);
  private

  public

  end;

var
  formMain: TformMain;

implementation

{$R *.lfm}

{ TformMain }

uses math;

function NetmaskDecimalConversion(netmaskValue : longint): byte;
var nmDecimal: byte;
begin
  case netmaskValue of
  1: nmDecimal := 128;
  2: nmDecimal := 192;
  3: nmDecimal := 224;
  4: nmDecimal := 240;
  5: nmDecimal := 248;
  6: nmDecimal := 252;
  7: nmDecimal := 254;
  8: nmDecimal := 255;
  else
    nmDecimal := 0;
  end;
  NetmaskDecimalConversion := nmDecimal;
end;

function NetmaskBinaryConversion(netmaskValue : longint): AnsiString;
var nmBinary: AnsiString;
begin
  case netmaskValue of
  1: nmBinary := '10000000';
  2: nmBinary := '11000000';
  3: nmBinary := '11100000';
  4: nmBinary := '11110000';
  5: nmBinary := '11111000';
  6: nmBinary := '11111100';
  7: nmBinary := '11111110';
  8: nmBinary := '11111111';
  else
    nmBinary := '????????';
  end;
  NetmaskBinaryConversion := nmBinary;
end;

function OctetValidation(inputOctet : longint; maxValue : longint): byte;
var testOctet   : longint;
    resultOctet : byte;
begin
  testOctet := inputOctet;
  if (testOctet < 0) then testOctet := 0;
  if (testOctet > maxValue) then testOctet := maxValue;
  resultOctet := testOctet;
  OctetValidation := resultOctet;
end;

function QWordToIPText(InputQword : QWord): AnsiString;
var tempQWord : QWord;
    IPtext1, IPtext2, IPtext3, IPtext4 : AnsiString;
begin
  tempQWord := InputQword shr 24;
  Str(tempQWord, IPtext1);
  tempQWord := (InputQword shr 16) mod 256;
  Str(tempQWord, IPtext2);
  tempQWord := (InputQword shr 8) mod 256;
  Str(tempQWord, IPtext3);
  tempQWord := InputQword mod 256;
  Str(tempQWord, IPtext4);
  QWordToIPText := IPtext1 + '.' + IPtext2 + '.' + IPtext3 + '.' + IPtext4;
end;

procedure TformMain.cbNetmaskChange(Sender: TObject);
var nmDecimal, nmBinary: AnsiString;
    nmValue: longint;
    nmText : AnsiString;
begin
  nmValue := IPnetmask.Value;
  if (nmValue < 0) then nmValue := 0;
  if (nmValue > 32) then nmValue := 32;
  case nmValue of
  0: begin
    nmDecimal := '0.0.0.0';
    nmBinary := '00000000.00000000.00000000.00000000';
  end;
  1..8: begin
    Str(NetmaskDecimalConversion(nmValue), nmText);
    nmDecimal := nmText + '.0.0.0';
    nmBinary := NetmaskBinaryConversion(nmValue) + '.00000000.00000000.00000000';
  end;
  9..16: begin
    nmValue := nmValue - 8;
    Str(NetmaskDecimalConversion(nmValue), nmText);
    nmDecimal := '255.' + nmText + '.0.0';
    nmBinary := '11111111.' + NetmaskBinaryConversion(nmValue) + '.00000000.00000000';
  end;
  17..24: begin
    nmValue := nmValue - 16;
    Str(NetmaskDecimalConversion(nmValue), nmText);
    nmDecimal := '255.255.' + nmText + '.0';
    nmBinary := '11111111.11111111.' + NetmaskBinaryConversion(nmValue) + '.00000000';
  end;
  25..32: begin
    nmValue := nmValue - 24;
    Str(NetmaskDecimalConversion(nmValue), nmText);
    nmDecimal := '255.255.255.' + nmText;
    nmBinary := '11111111.11111111.11111111.' + NetmaskBinaryConversion(nmValue);
  end;
  else
    nmDecimal := 'Error';
    nmBinary := 'Error';
  end;
  tbNetmaskDecimal.Text := nmDecimal;
  tbNetmaskBinary.Text := nmBinary;
end;

procedure TformMain.btnQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TformMain.btnHelpClick(Sender: TObject);
var helpStr : AnsiString;
begin
  helpStr := 'file://'+ExtractFilePath(ParamStr(0))+'help.html';
  OpenURL(helpStr);
end;

function MaskThis(IPval: byte; NMval: byte): byte;
var MaskValue : byte;
begin
  case NMval of
  1: MaskValue := 128;
  2: MaskValue := 192;
  3: MaskValue := 224;
  4: MaskValue := 240;
  5: MaskValue := 248;
  6: MaskValue := 252;
  7: MaskValue := 254;
  8: MaskValue := 255;
  end;
  MaskThis := IPval and MaskValue;
end;

{ Determine network class }
function NetClassFN(IPoct1: byte): AnsiString;
var tempText : AnsiString;
begin

  tempText := 'Network Class: Undefined';
  if ((IPoct1 and 128) = 0) then tempText := 'Network Class: A';
  if ((IPoct1 and 192) = 128) then tempText := 'Network Class: B';
  if ((IPoct1 and 224) = 192) then tempText := 'Network Class: C';
  if ((IPoct1 and 240) = 224) then tempText := 'Network Class: D';
  if ((IPoct1 and 240) = 240) then tempText := 'Network Class: E';
  NetClassFN := tempText;
end;

{ Determine the network type }
function NetTypeFN(IPoct1: byte; IPoct2: byte; IPoct3: byte; IPoct4: byte): AnsiString;
var NetTypeText : AnsiString;
begin
  NetTypeText := 'Internet.';
  case IPoct1 of
  0   : NetTypeText := 'Software: This host on this network.';
  10  : NetTypeText := 'Private network.';
  100 : if ((IPoct2 >= 64) and (IPoct2 <= 127)) then NetTypeText := 'Private: Shared Address Space (CG-NAT).';
  127 : NetTypeText := 'Host: Loopback.';
  169 : if (IPoct2 = 254) then NetTypeText := 'Subnet: Link Local Address.';
  172 : if ((IPoct2 >= 16) and (IPoct2 <= 31)) then NetTypeText := 'Private network.';
  192 : case IPoct2 of
    0  : begin
      if (IPoct3 = 0) then NetTypeText := 'Private: IETF Protocol Assignments.';
      if (IPoct3 = 2) then NetTypeText := 'Documentation: Assigned as TEST-NET-2, documentation and examples.';
    end;
    88  : if (IPoct3 = 99) then NetTypeText := 'Internet: 6to4 Relay Anycast.';
    168 : NetTypeText := 'Private network.';
  end;
  198 : case IPoct2 of
    18..19 : NetTypeText := 'Private: Benchmarking.';
    51     : if (IPoct3 = 100) then NetTypeText := 'Documentation: Assigned as TEST-NET-2, documentation and examples.';
  end;
  203 : if ((IPoct2 = 0) and (IPoct3 = 113)) then NetTypeText := 'Documentation: Assigned as TEST-NET-3, documentation and examples.';
  233 : if ((IPoct2 = 252) and (IPoct3 = 0)) then NetTypeText := 'Documentation: Assigned as MCAST-TEST-NET, documentation and examples.';
  255 : if ((IPoct2 = 255) and (IPoct3 = 255) and (IPoct4 = 255)) then NetTypeText := 'Subnet: Limited Broadcast.';
  end;
  NetTypeFN := 'Network Type:  ' + NetTypeText;
end;

procedure TformMain.btnCalculateClick(Sender: TObject);
var Octet1    : byte;
    Octet2    : byte;
    Octet3    : byte;
    Octet4    : byte;
    CIDR      : byte;
    IPqword   : QWord;
    NMqword   : QWord;
    NetQword  : QWord;
    BCqword   : QWord;
    tempQword : QWord;
    tempText1 : AnsiString;
    tempText2 : AnsiString;
begin
  cbNetmaskChange(Sender);
  memoDetails.Clear;
  memoDetails.Append('');
  Octet1 := OctetValidation(IPoctet1.Value, 255);
  Octet2 := OctetValidation(IPoctet2.Value, 255);
  Octet3 := OctetValidation(IPoctet3.Value, 255);
  Octet4 := OctetValidation(IPoctet4.Value, 255);
  CIDR := OctetValidation(IPnetmask.Value, 32);
  Str(CIDR, tempText1);
  IPqword := ((Octet1*256 + Octet2)*256 + Octet3)*256 + Octet4;
  NMqword := (2**32) - (Qword(2)**QWord((32-CIDR)));
  NetQword := IPqword and NMqword;
  BCqword := NetQword + (QWord(2)**QWord((32-CIDR))) - 1;
  { Display the results }
  tempText1 := 'Network:       ' + QWordToIPText(NetQword) + ' / ' + tempText1;
  memoDetails.Append(tempText1);
  case CIDR of
  0..30 : begin
    tempText1 := 'Broadcast:     ' + QWordToIPText(BCqword);
    memoDetails.Append(tempText1);
    Str((32-CIDR), tempText1);
    tempText1 := 'Host bits:     ' + tempText1;
    memoDetails.Append(tempText1);
    tempQword := (QWord(2)**QWord((32-CIDR))) - 2;
    Str(tempQword, tempText1);
    Str((32-CIDR), tempText2);
    tempText1 := 'Maximum Hosts: ' + tempText1 + ' (2^'+tempText2+' - 2)';
    memoDetails.Append(tempText1);
    tempText1 := 'Host IP Range: ' + QWordToIPText((NetQword+1)) + ' - ' + QWordToIPText((BCqword-1));
    memoDetails.Append(tempText1);
    if (IPqword = NetQword) then memoDetails.Append('IP Type:       Network')
    else if (IPqword = BCqword) then memoDetails.Append('IP Type:       Broadcast address')
    else memoDetails.Append('IP Type:       Host address');
  end;
  31 : begin
    tempText1 := 'Broadcast:     Not Applicable (Point-to-Point link)';
    memoDetails.Append(tempText1);
    memoDetails.Append('Host bits:     1');
    memoDetails.Append('Maximum Hosts: 2');
    tempText1 := 'Host IP Range: ' + QWordToIPText(NetQword) + ' - ' + QWordToIPText((NetQword+1));
    memoDetails.Append(tempText1);
    memoDetails.Append('IP Type:       Host address');
  end;
  32 : begin
    tempText1 := 'Broadcast:     Not Applicable (Single IP Address)';
    memoDetails.Append(tempText1);
    memoDetails.Append('Host bits:     0');
    memoDetails.Append('Maximum Hosts: 1');
    tempText1 := 'Host IP Range: ' + QWordToIPText(IPqword);
    memoDetails.Append(tempText1);
    memoDetails.Append('IP Type:       Host address');
  end;
  end;
  memoDetails.Append(NetClassFN(Octet1));
  memoDetails.Append(NetTypeFN(Octet1, Octet2, Octet3, Octet4));
end;

end.

